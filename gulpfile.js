    const { watch, parallel } = require('gulp');   
    const browserSync = require('browser-sync').create(); 

    function browser_sync() {
        browserSync.init({
            server: {
                host: "./"
            }
        });
    }

    function watch_files() {
        var watcher = watch(["index.html", "resources/css/*.css", "resources/js/*.js"]);

        watcher.on('change', function(path, stats) {
            console.log(`File ${path} was changed`);
            browserSync.reload();
        });
    }

    exports.default = parallel(browser_sync, watch_files);