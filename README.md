## Project ini memerlukan NPM (Package Manager)

1. Install NPM (Package Manager) terlebih dahulu
2. Dengan Command Prompt (CMD) atau Terminal, arahkan ke folder project
    - `cd path/to/folder/project`
3. Jika sudah terinstall NPM dan sudah berada di folder project jalankan perintah
    - `npm update`